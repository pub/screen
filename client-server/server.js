//
// server.js
//    streaming content server
//    Neil Gershenfeld
//    2/12/17
// todo
//    standalone client content push
//    browser client content push
//    https
//    parse command line
//    add authentication
//    interframe compression
//    include audio
// uses maim for screen capture:
//    https://github.com/naelstrof/maim
//    https://github.com/naelstrof/maim/archive/master.zip
//    install libimlib2-dev libxrandr-dev libxfixes-dev cmake
//
// command line
//
if (process.argv.length != 3) {
   console.log("node server.js port")
   process.exit()
   }
var port = parseInt(process.argv[2])
//
// requires
//
const http = require('http')
const fs = require('fs')
//
// globals
//

//
// initialization
//
var img = null
//
// start tcp server
//
//
// start http server
//
http.createServer(function(request,response) {
   var headers = request.headers
   var method = request.method
   var url = request.url
   if (url == '/') {
      fs.readFile('client.html',function(err,data){
         response.writeHead(200,{'Content-Type':'text/html'});
         response.end(data)
         })
      }
   else if (url == '/img') {
      //requests.push(response)
      response.writeHead(200,{'Content-Type':'application/octet-stream'})
      response.end(img,'binary')
      }
   else if (url == '/initimg') {
      response.writeHead(200,{'Content-Type':'application/octet-stream'})
      response.end(img,'binary')
      }
   else if (url == '/server.js') {
      fs.readFile('server.js',function(err,data){
         response.writeHead(200,{'Content-Type':'text/plain'});
         response.end(data)
         })
      }
   else if (url == '/client.js') {
      fs.readFile('client.js',function(err,data){
         response.writeHead(200,{'Content-Type':'text/plain'});
         response.end(data)
         })
      }
   else if (url == '/client.html') {
      fs.readFile('viewer.html',function(err,data){
         response.writeHead(200,{'Content-Type':'text/plain'});
         response.end(data)
         })
      }
   else if (url == '/stats') {
      var resp = "<html>"
      resp += "<body>"
      resp += "stats"
      //resp += "last: "+last+"<br>"
      //resp += "max: "+max+"<br>"
      response.writeHead(200,{'Content-Type':'text/html'});
      response.end(resp)
      }
  }).listen(port)
//
// start event loop
//

//
// event loop
//
