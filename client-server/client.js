//
// client.js
//    streaming content Node client
//    Neil Gershenfeld
//    2/12/17
// todo
// uses maim for screen capture:
//    https://github.com/naelstrof/maim
//    https://github.com/naelstrof/maim/archive/master.zip
//    install libimlib2-dev libxrandr-dev libxfixes-dev cmake
//
// command line
//
if (process.argv.length != 4) {
   console.log("node client.js server_address server_port")
   process.exit()
   }
var address = parseInt(process.argv[2])
var port = parseFloat(process.argv[3])
//
// requires
//
const spawn = require('child_process').spawn;
const spawnSync = require('child_process').spawnSync
const fork = require('child_process').fork
const fs = require('fs')
const http = require('http')
//
// globals
//



